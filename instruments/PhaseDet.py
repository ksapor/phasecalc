#-------------------------------------------------------------------------------
# Name:        PhaseDet
# Purpose:     A class used for interpolating measurement data acquired from phase detector.
#
# Author:      Kamil Sapor
#
# Created:     16-04-2017
#-------------------------------------------------------------------------------
import numpy as np
import pylab
import os

class PhaseDet():
    def __init__(self, detector_filename):
        self.LoadDetectorData(detector_filename)

    def LoadDetectorData(self, detector_filename):
        if detector_filename != None:
            data = np.load(detector_filename)     #detector data
            
            self.degree = data['degree']
            self.voltage = data['voltage']
            
            max_marker = np.argmax(self.voltage)                    #index of the maximum voltage value in detector's characteristic
            #===================================
            #max_marker should be somewhere near half of the data vector's length, since characteristic is symmetrical.
            #===================================
            
            self.voltage_positive = self.voltage[0:max_marker]      #dividing the data vector into positive and negative edge parts
            self.degree_positive = self.degree[0:max_marker]
            
            self.voltage_negative = self.voltage[max_marker:]
            self.degree_negative = self.degree[max_marker:]
             
            self.voltage_negative = self.voltage_negative[::-1]
            self.degree_negative = self.degree_negative[::-1]
            
            #self.PlotDetectorData() # for debugging purposes
            
            print('Detector\'s data file %s loaded successfully.' %detector_filename)
            
        else:
            print('Filename for detector data not specified! Using default data.')
            self.voltage_positive = np.linspace(0, 1.8, 5000, True)
            self.degree_positive = np.linspace(0, 180, 5000, True)
            
            self.voltage_negative = np.linspace(1.8, 0, 5000, True)
            self.degree_negative = np.linspace(180, 360, 5000, True)
            
            self.voltage_negative = self.voltage_negative[::-1]
            self.degree_negative = self.degree_negative[::-1]
            
        self.voltage_mag = np.linspace(0, 1.8, 5000, True)
        self.decibel_mag = np.linspace(-30, 30, 5000, True)
    
    def CalculatePhase(self, detectorSlopeType, phaseData):
        if detectorSlopeType == 'Positive':
            phaseOut = np.interp(phaseData, self.voltage_positive, self.degree_positive)
        elif detectorSlopeType == 'Negative':
            phaseOut = np.interp(phaseData, self.voltage_negative, self.degree_negative)

        return phaseOut

    def CalculateMagnitude(self, magnitudeData):
        magnitudeOut = np.interp(magnitudeData, self.voltage_mag, self.decibel_mag)
        
        return magnitudeOut
    
    def PlotDetectorData(self):
        fig1 = pylab.figure()
        ax1 = fig1.add_subplot(1,1,1)
            
        ax1.plot(self.degree_positive, self.voltage_positive, 'b-', label = 'Rising edge')
        ax1.plot(self.degree_negative, self.voltage_negative, 'r-', label = 'Falling edge')
           
        pylab.title('Phase detector\'s characteristic')
        pylab.legend(loc='best')
           
        pylab.grid()
        pylab.show()