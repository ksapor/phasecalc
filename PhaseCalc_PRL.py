#-------------------------------------------------------------------------------
# Name:        PhaseCalc_main
# Purpose:     Script for plotting the measurement data acquired from phase detector.
#
# Author:      Kamil Sapor
#
# Created:     16-04-2017
#-------------------------------------------------------------------------------

import os
import pylab
import copy
import numpy as np

from PhaseData import MeasData, PhaseData, PhaseDataInitStruct
import PhasePlot as phpl

#== CONSTANTS ==================================================================
FREQ = 704.00          #Needs to be specified in MHz
DETECTOR_FILENAME = None#'GreenDetNew_704MHz.npz'
DF1 = 'PRL-SB_704M_68h_11_23_2018_data.txt'
#===============================================================================

#== PATHS ======================================================================
CWD = os.getcwd()
DATAPATH = '%s\\data'%CWD
PICPATH = '%s\\pictures'%CWD
DETPATH = '%s\\instruments'%CWD
#===============================================================================

#== MAIN =======================================================================
pdinit = PhaseDataInitStruct()                 
pdinit.datapath = DATAPATH
pdinit.detpath = DETPATH
pdinit.detFilename = DETECTOR_FILENAME
#===============================================================================

#== DATA COLUMNS ===============================================================
pdinit.phaseColumn = 6
pdinit.magnitudeColumn = 1
pdinit.tempColumn1 = 4
pdinit.tempColumn2 = 5
pdinit.humidityColumn = 1
pdinit.voltageColumn = 1
pdinit.pressureColumn = 1
#===============================================================================

#== PHASEPLOT PARAMS ===========================================================
timeInt = 10                # interwal miedzy pomiarami
diffWindow = 50             # okno filtru dla rozniczkowania
hours_per_slope = 4         # czas trwania jednego ZBOCZA (w godzinach)
slopes_per_cycle = 4        # czas trwania jednego CYKLU (w ZBOCZACH)
all_cycles = 4              # liczba wszystkich CYKLI temperaturowych

Nslope = hours_per_slope*int(3600/timeInt)
Ncycle = Nslope*slopes_per_cycle
n_start = 110                       
n_stop = all_cycles*Ncycle + Nslope
#===============================================================================

#== OTHER PARAMS ===============================================================
pdinit.windowSize = 50      # okno filtru dla danych ogolnych
pdinit.phaseWindow = 50     # okno filtru dla fazy
pdinit.nstart = n_start

pdinit.freq = FREQ
pdinit.timeInt = timeInt
pdinit.phaseSlopeType = 'Positive'  # zbocze detektora, 'Positive' lub 'Negative' [+/- 10mV/st]
pdinit.length_cable = 1
#===============================================================================

#== Loading data ===============================================================
pdinit.datafile_name = DF1
pd1 = PhaseData(pdinit)
pd1.temp1.setLabel('T_SB')
pd1.temp2.setLabel('Oven2')

dataset = [pd1.phasedeg, pd1.temp1, pd1.temp2]      # wektory danych do rysowania dla metody DataVsTime
#===============================================================================

#== Preparing datasets =========================================================
pp = phpl.PhasePlot(n_start, n_stop, diffWindow, picpath = PICPATH)
#===============================================================================

#== DataVsTime =================================================================
DUTNAME = 'PRL SB 704M\'s OUT'
tit1 = "Phase difference and temperature changes over time.\n%s at %.2fMHz."%(DUTNAME, FREQ)
pp.DataVsTime(pd1.time, dataset, title = tit1, filename = 'prl_sb_704M.png')
#===============================================================================

#== DataVsData =================================================================
pp.DataVsData(pd1.temp1, pd1.phasedeg, x_range = [17, 45], plot_nums = [1, 8])
#===============================================================================

#== CoefficientCalc ============================================================
pp.CoefficientCalc(pd1.temp1, pd1.phasedeg, x_range = [17, 45], plot_nums = [1, 8])
#===============================================================================

pylab.show()
#===============================================================================