#-------------------------------------------------------------------------------
# Name:        PhaseData
# Purpose:     Classes used for processing and storing 
#              measurement data acquired from phase detector
#              and other devices.
#
# Author:      Kamil Sapor
#
# Version:     2.0
# Created:     17-09-2018
#-------------------------------------------------------------------------------
import numpy as np
import os, sys
import pylab
import scipy.signal as signal
from enum import Enum

#===============================================================================
class Range(Enum):
    Low = 1
    Rise = 2
    High = 3
    Fall = 4
#===============================================================================

#===============================================================================
class RawData():
    """
    Base class for storing data.
    It stores floating-point data array, along with
    string variables describing data type and appropriate SI unit.
    It is used as a basis for TimeData and MeasData classes.
    """
    def __init__(self, data, datatype, dataunit):
        self.data = np.float64(data)
        self.datatype = str(datatype)
        self.dataunit = str(dataunit)
    
    def __len__(self):
        return len(self.data)
    
    def getData(self):
        return self.data
    
    def getType(self):
        return self.datatype
    
    def getUnit(self):
        return self.dataunit
        
    def scaleData(self, factor, new_unit):
        """
        This method allows scaling the data saved in MeasData object.
        For example, if object named phaseData stores data of type 'Phase'
        with unit 'ps', then calling this method like this:
        #======================================
        phaseData.scaleData( 1e-3, 'ns' )
        #======================================
        will scale the phaseData by a factor of 1e-3 and change its unit to 'ns'.
        
        One has to be careful, as units are set manually and are not checked
        for consistency with the scaling factor.
        """
        self.data = self.data*factor
        self.dataunit = new_unit
        return self
#===============================================================================

#===============================================================================
class TimeData(RawData):
    """
    Main class for storing time data (and sample numbers).
    It stores floating-point data array, along with
    string variables describing data type and appropriate SI unit.
    """
    def __getitem__(self, index):
        return TimeData( self.data[index], self.datatype, self.dataunit)
    pass
#===============================================================================

#===============================================================================
class MeasData(RawData):
    """
    Main class for storing measurement data.
    It stores floating-point data array, along with
    string variables describing data type and appropriate SI unit.
    """
    def __init__(self, data, datatype, dataunit, label = None):
        RawData.__init__(self, data, datatype, dataunit)
        
        if(label):
            self.label = str(label)
        else:
            self.label = self.datatype
    
    def __getitem__(self, index):
        return MeasData( self.data[index], self.datatype, self.dataunit, self.label )
    
    def __sub__(self, other):
        if(type(other) is not MeasData):
            raise TypeError('Cannot subtract different kind of object from MeasData object.')
        
        N = len(self)
        M = len(other)
        
        if(N > M):
            return MeasData( self.data[0:M] - other.data[0:M], self.datatype, self.dataunit, self.label)
        elif(M > N):
            return MeasData( self.data[0:N] - other.data[0:N], self.datatype, self.dataunit, self.label)
        else:
            return MeasData( self.data - other.data, self.datatype, self.dataunit, self.label)
    
    def getLabel(self):
        return self.label
    
    def setLabel(self, new_label):
        """
        This method allows setting a label for the data stored in MeasData object.
        Set label can be used when plotting multiple MeasData objects.
        """
        self.label = str(new_label)
        return self
        
    def interpData(self, time, new_time):
        """
        This method allows to interpolate stored data to a different
        set of x-coordinates passed as TimeData object - 'new_time'.
        Old x-coordinates are passed as 'time' argument.
        Useful when comparing datasets acquired from different measurements 
        with different timescales. Both arguments has to have 
        same datatype and dataunit - scaleData() method may be useful.
        """
        if ( type(time) is not TimeData ) or ( type(new_time) is not TimeData ):
            raise TypeError('Both arguments have to be objects of type TimeData.')
        if( len(time) != len(self) ):
            raise ValueError('time argument must have same datasize as invoked MeasData object.')
        
        ttype0 = [ self.time.getType(), self.time.getUnit() ]
        ttype = [ new_time.getType(), new_time.getUnit() ]
        if(ttype0 != ttype):
            raise TypeError('Given arguments have different datatypes or/and dataunits. Cannot interpolate.')
        else:
            return np.interp(new_time.getData(), time.getData(), self.data.getData())
#===============================================================================

#===============================================================================
class PhaseDataInitStruct():
    def __init__(self):
        self.datafile_name = None                   # Name of the file containing data to be processed.
        self.datapath = '%s\\data'%os.getcwd()          # Location of the datafile
        self.detpath = '%s\\instruments'%os.getcwd()    # Location, where phase detector data is stored.
        self.detFilename = None                     # Name of the file containing phase detector data for interpolation purposes.
        #=======================================================================
        # The part below specifies the columns in which specific measurement data is stored.
        #=======================================================================
        self.phaseColumn = 1
        self.tempColumn1 = 1
        self.tempColumn2 = 1
        self.magnitudeColumn = None
        self.humidityColumn = None
        self.pressureColumn = None
        #=======================================================================
        #=======================================================================
        self.windowSize = 50
        self.phaseWindow = 50
        self.nstart = 0
        
        self.freq = 1300.00
        self.timeInt = 0                    # Specifies the time interval between every measurement
        self.phaseSlopeType = 'Positive'    # Specifies on which edge of the detector's characteristic the measurements were done
        self.length_cable = 0               # Specifies length of the measured cable (in meters) for the purpose of data normalization; if a device was measured, set it to 1.
#===============================================================================

#===============================================================================
class PhaseData():
    def __init__(self, pdinit):
        if pdinit != None:
            #== Loading instrument data ========================================
            startDir = os.getcwd()
            os.chdir(pdinit.detpath)
            #detector_path = os.getcwd()
            sys.path.append(pdinit.detpath)
            #os.chdir(pdinit.detpath)
            from PhaseDet import PhaseDet
            self.ad8302 = PhaseDet(pdinit.detFilename)
            os.chdir(startDir)
            #===================================================================
            
            #== Setting variables ==============================================
            self.windowSize = pdinit.windowSize
            self.phaseWindow = pdinit.phaseWindow
            self.nstart = pdinit.nstart
            
            self.freq = pdinit.freq
            self.length_cable = pdinit.length_cable
            #===================================================================
            
            #== Processing data ================================================
            data = pylab.genfromtxt('%s\\%s'%(pdinit.datapath, pdinit.datafile_name))
            
            self.phasetime = self.PhaseTime(pdinit.phaseSlopeType, np.float64( data[:, pdinit.phaseColumn - 1] ) )
            self.phasedeg = self.PhaseDeg(pdinit.phaseSlopeType, np.float64( data[:, pdinit.phaseColumn - 1] ) )
            self.temp1 = self.Temperature( np.float64( data[:, pdinit.tempColumn1 - 1] ) )
            if(pdinit.tempColumn2):
                self.temp2 = self.Temperature( np.float64( data[:, pdinit.tempColumn2 - 1] ) )
            if(pdinit.magnitudeColumn):
                self.magnitude = self.Magnitude( np.float64( data[:, pdinit.magnitudeColumn - 1] ) )
            if(pdinit.humidityColumn):
                self.humidity = self.Humidity( np.float64( data[:, pdinit.humidityColumn - 1] ) )
            if(pdinit.pressureColumn):
                self.pressure = self.Pressure( np.float64( data[:, pdinit.pressureColumn - 1] ) )
            #===================================================================
            
            #== Generating time vector =========================================
            N = len(data[:, 1])
            Nsamp = np.linspace(0, N-1, N)
            timeDiv = np.float64( 3600 / pdinit.timeInt )
            self.time = TimeData( Nsamp / timeDiv, 'Time', 'h')
            self.samples = TimeData( Nsamp, 'Samples', 'No.' )
            
            #===================================================================
        else:
            raise ValueError('Initializing structure was not given or was empty!')

    def Temperature(self, tempData):
        return MeasData( self.Filter( tempData, self.windowSize), 'Temperature', '$^o$C')

    def PhaseDeg(self, detectorEdge, phaseVoltage):
        """
        This method calculates phase values from measured AD8302 output voltage.
        Detector characteristic's edge must be specified (either 'Positive' or 'Negative').
        Output vector is scaled in degrees and normalized to cable length, if it was given. 
        If DUT is not a cable, then lenght_cable should be set to 1.
        """
        phase = self.ad8302.CalculatePhase(detectorEdge, phaseVoltage)
        phase = phase / self.length_cable
        phase = phase - phase[self.nstart]
        return MeasData( self.Filter(phase, self.phaseWindow), 'Phase', 'deg')

    def PhaseTime(self, detectorEdge, phaseVoltage):
        """
        This method calculates phase values from measured AD8302 output voltage.
        Detector characteristic's edge must be specified (either 'Positive' or 'Negative').
        Output vector is scaled in picoseconds and normalized to cable length, if it was given. 
        If DUT is not a cable, then lenght_cable should be set to 1.
        """
        phase = self.ad8302.CalculatePhase(detectorEdge, phaseVoltage)
        phase = phase * 1000 / (0.36 * self.freq)             
        phase = phase / self.length_cable
        phase = phase - phase[self.nstart]
        return MeasData( self.Filter(phase, self.phaseWindow), 'Phase', 'ps')
    
    def Magnitude(self, magVoltage):
        """
        This method calculates phase values from measured AD8302 output voltage.
        Output vector is scaled in decibels [dB].
        """
        magnitude = self.ad8302.CalculateMagnitude(magVoltage)
        return MeasData( self.Filter(magnitude, self.windowSize), 'Magnitude', 'dB')
    
    def Humidity(self, sensVoltage):
        """
        This method calculates humidity values from measured HIH-4000 output voltage.
        Output vector is scaled in percents of relative humidity [%RH].
        """
        humidity = (sensVoltage - 0.826)/0.031483                    #for HIH-4000 sensor powered by 5V supply
        return MeasData( self.Filter(humidity, self.windowSize), 'Humidity', '%RH')

    def Pressure(self, sensVoltage):
        """
        This method calculates pressure values from measured MPX5100AP output voltage.
        Output vector is scaled in hPa?
        """
        pressure = (sensVoltage/5 + 0.095)/0.009
        return MeasData( self.Filter(pressure, self.windowSize), 'Pressure', 'hPa')
    
    @staticmethod
    def Filter(data, windowSize):
        """
        This method serves as wrapper for low-pass filtering - more precisely,
        it is an averaging filter and it needs to have windowSize specified.
        """
        filteredData = signal.lfilter( np.ones(windowSize)/windowSize, 1, data )
        return filteredData
    
    @staticmethod
    def DiffCalculate(nominator, denominator, diffWindow):
        """
        This method calculates differences between neighboring elements of an array.
        The output array is also filtered before being returned.
        """
        diff = np.divide(np.diff(nominator), np.diff(denominator))
        return PhaseData.Filter(diff, diffWindow)

if __name__ == '__main__':
    pass

