#-------------------------------------------------------------------------------
# Name:        PhasePlot
# Purpose:     Classes and methods used for plotting measurement data acquired from phase detector
#              and processed with PhaseData class.
#
# Author:      Kamil Sapor
#
# Version:     1.0
# Created:     17-09-2018
#-------------------------------------------------------------------------------
import numpy as np
import os
import pylab
import scipy.stats as stats
#import scipy.signal as signal
from PhaseData import TimeData, MeasData, PhaseData
from enum import Enum

#===============================================================================
class Range(Enum):
    Low = 1
    Rise = 2
    High = 3
    Fall = 4
#===============================================================================

#===============================================================================
class PhasePlot():
    def __init__(self, nstart, nstop, diffWindow = 10, picpath = None, datapath = None):
        #== Assertions =========================================================
        if( type(nstart) is not type(int()) ) or ( type(nstop) is not type(int()) ):
            raise TypeError('nstart and nstop arguments must be of type int.')
        else:
            self.nstart = nstart
            self.nstop = nstop
        
        if( type(diffWindow) is not type(int()) ):
            raise TypeError('diffWindow argument must be of type int.')
        else:
            self.diffWindow = diffWindow
        #=======================================================================
        
        #== Checking and creating directories ==================================
        if(picpath):
            self.picpath = str(picpath)
            if( not os.path.exists(self.picpath) ):
                os.makedirs(self.picpath, exist_ok = True)
                print('Created directory %s'%self.picpath)
        else:
            self.picpath = None
            
        if(datapath):
            self.datapath = str(datapath)
            if( not os.path.exists(self.datapath) ):
                os.makedirs(self.datapath, exist_ok = True)
                print('Created directory %s'%self.datapath)
        else:
            self.datapath = None
        #=======================================================================
        
        #== Color arrays for DataVsTime method =================================
        self.renewColors()
        #=======================================================================
        
    def renewColors(self):
        self.colors1 = pylab.rcParams['axes.prop_cycle'].by_key()['color']
        self.colors2 = ['r', 'g', 'c', 'r']
        
        self.Ncolor1 = len(self.colors1)
        self.Ncolor2 = len(self.colors2)
        
    def DataVsTime(self, time, dataset, title = None, filename = None):
        #== Assertions =========================================================
        N = len(dataset)
        #if( N < 1 ) or ( N > self.Ncolor):
            #raise AssertionError('Dataset has to be an array of size between 1 and %d. Current size: %d'%(self.Ncolor, N))
        
        for i in range(N):
            if( type(dataset[i]) is not MeasData ):
                raise TypeError('Every element of dataset has to be an object of MeasData class.')
            
        if( type(time) is not TimeData ):
            raise TypeError('time argument has to be an object of TimeData class.')
        #=======================================================================
    
        #== Checking number of datatypes =======================================
        datatypes = []
        for i in range(N):
            dtype = [dataset[i].getType(), dataset[i].getUnit()]
            if( not i ):
                datatypes.append( dtype )
            else:
                typefound = 0
                for j in range( len(datatypes) ):
                    if( dtype == datatypes[j] ):
                        typefound = 1
                        break
                if(not typefound):
                    datatypes.append( dtype )
        #=======================================================================
        
        #== Plotting datasets ==================================================
        fig = pylab.figure()
        
        Ntype = len(datatypes)
        if(Ntype > 2):
            raise TypeError('Too many datatypes in dataset argument. Can plot maximum of 2 datatypes.')
        
        elif(Ntype == 1):
            ax1 = fig.add_subplot(1, 1, 1)
            for i in range(N):
                ax1.plot(time.getData()[self.nstart : self.nstop], dataset[i].getData()[self.nstart : self.nstop], label = dataset[i].getLabel(), color = self.colors1.pop(0) )
            ax1.set_xlabel( '%s [%s]'%(time.getType(), time.getUnit()) )
            ax1.set_ylabel( '%s [%s]'%(dataset[0].getType(), dataset[0].getUnit()), color = 'k' )
            ax1.grid()
            ax1.legend(loc = 'best')
            
        elif(Ntype == 2):
            idx1 = []
            idx2 = []
            for i in range(N):
                dtype = [dataset[i].getType(), dataset[i].getUnit()]
                if(dtype == datatypes[0]):
                    idx1.append(i)
                elif(dtype == datatypes[1]):
                    idx2.append(i)
            
            #===================================================================
            Ntype1 = len(idx1)
            Ntype2 = len(idx2)
            if(Ntype1 > int(self.Ncolor1) ):
                raise AssertionError('Can only plot maximum of %d MeasData objects of first datatype at once.'%int(self.Ncolor1))
            if(Ntype2 > int(self.Ncolor2) ):
                raise AssertionError('Can only plot maximum of %d MeasData objects of second datatype at once.'%int(self.Ncolor2))
            #===================================================================
            
            ax1 = fig.add_subplot(1, 1, 1)
            ax2 = ax1.twinx()
            for i in idx1:
                plt1 = ax1.plot(time.getData()[self.nstart : self.nstop], dataset[i].getData()[self.nstart : self.nstop], label = dataset[i].getLabel(), color = self.colors1.pop(0) )
            ax1.grid()
            ax1.set_xlabel( '%s [%s]'%(time.getType(), time.getUnit()) )
            if(Ntype1 == 1):
                ax1.set_ylabel( '%s [%s]'%(dataset[i].getType(), dataset[i].getUnit()), color = plt1[-1].get_color() )
            else:
                ax1.set_ylabel( '%s [%s]'%(dataset[i].getType(), dataset[i].getUnit()), color = 'k' )
                ax1.legend(loc = 'upper left')
            
            for i in idx2:
                plt2 = ax2.plot(time.getData()[self.nstart : self.nstop], dataset[i].getData()[self.nstart : self.nstop], label = dataset[i].getLabel(), color = self.colors2.pop(0) )
            
            if(Ntype2 == 1):
                ax2.set_ylabel( '%s [%s]'%(dataset[i].getType(), dataset[i].getUnit()), color = plt2[-1].get_color() )
            else:
                ax2.set_ylabel( '%s [%s]'%(dataset[i].getType(), dataset[i].getUnit()), color = 'k' )
                ax2.legend(loc = 'upper right')
                
            #== Calculating correlations =======================================
            for i in idx1:
                for j in idx2:
                    corr12 = np.corrcoef( x = [ dataset[i].getData()[self.nstart : self.nstop], dataset[j].getData()[self.nstart : self.nstop] ] )
                    print('Correlation of %s and %s:'%( dataset[i].getLabel(), dataset[j].getLabel() ) )
                    print(corr12)
            #===================================================================
        self.renewColors()
        #=======================================================================
        
        #== Adding title and saving to file (optional) =========================
        if(title):
            pylab.title(title)
        pylab.tight_layout()
            
        if(filename and self.picpath):
            pylab.savefig( '%s\\%s'%(self.picpath, filename) )
        #=======================================================================
    
    @staticmethod
    def FindEdges(data, start, stop, lo_lim, hi_lim):
        # Initialize variables
        i = 0
        edge_idx = []
        
        # Check if limits are valid
        if (data[start] < lo_lim ):
            flag = Range.Low
        elif (data[start] > hi_lim ):
            flag = Range.High
        else: 
            raise ValueError('Starting value is inside searching limits! Cannot determine edge!')
            
        if( stop > len(data) ):
            stop = len(data)
            
        # Search for edge border indices
        for i in np.linspace( start, stop - 1, stop - start, dtype = int):
            if( flag == Range.Low ):
                if( data[i] >= lo_lim ):
                    edge_idx.append(i)
                    flag = Range.Rise
            
            elif( flag == Range.Rise ):
                if(data[i] >= hi_lim ):
                    edge_idx.append(i)
                    flag = Range.High
            
            elif( flag == Range.High ):
                if(data[i] <= hi_lim ):
                    edge_idx.append(i)
                    flag = Range.Fall
            
            elif( flag == Range.Fall ):
                if(data[i] <= lo_lim ):
                    edge_idx.append(i)
                    flag = Range.Low
        return edge_idx
    
    @staticmethod
    def CreateLabels(limit):
        label_suffix = ['$^{st}$ ', '$^{nd}$ ', '$^{rd}$ ', '$^{th}$ ']
        label_edge = ['increase', 'decrease']
        
        labels = [None] * int(limit / 2)
        
        for i in range( int(limit / 2) ):
            if(i < 6):
                labels[i] = str( np.int(np.floor(i/2)) + 1 ) + label_suffix[ np.int(np.floor(i/2)) ] + label_edge[ i % 2 ]
            else:
                labels[i] = str( np.int(np.floor(i/2)) + 1 ) + label_suffix[ 3 ] + label_edge[ i % 2 ]
        
        return labels
    
    def DataVsData(self, x, y, x_range, plot_nums = [1, 2], title = None, filename = None ):
        #== Assertions =========================================================
        if ( type(x) is not MeasData ) or ( type(y) is not MeasData ):
            raise TypeError('x and y have to be objects of type MeasData.')
        #=======================================================================
        
        #== Finding edges ======================================================
        edge_idx = self.FindEdges(x.getData(), self.nstart, self.nstop, x_range[0], x_range[1])                
        limit = len(edge_idx)
        
        if (limit % 2 != 0):
            raise ValueError('edge_idx must be an array of integers containing an even number of elements!')
        if ( np.max(plot_nums) > limit/2 ):
            print ('max(plot_nums) = %d'%np.max(plot_nums))
            print ('Slopes = %d'%(limit/2))
            raise ValueError('plot_nums elements have to be smaller than number of slopes in x_data!')
        
        labels = self.CreateLabels(limit)
        
        i = 0
        plot_start = plot_nums[0]
        plot_stop = plot_nums[1]
        #=======================================================================
        
        #== Plotting data ======================================================
        fig = pylab.figure()
        ax1 = fig.add_subplot(1,1,1)
        
        slope = [0.0] * int(limit / 2)
        
        for i in np.linspace(plot_start, plot_stop, plot_stop - plot_start + 1, endpoint = True, dtype = int) :
            x_data = x.getData()[edge_idx[(i-1)*2] : edge_idx[(i-1)*2 + 1]]
            y_data = y.getData()[edge_idx[(i-1)*2] : edge_idx[(i-1)*2 + 1]]
            if(i % 2 != 0):
                y_data = y_data - y_data[0]
            else:
                y_data = y_data - y_data[-1]
            
            ax1.plot(x_data, y_data)
    
            [slope[i-1], intercept, corr, p_value, std_err] = stats.linregress(x_data, y_data)
    
            #print (labels[i-1])
            #print ('Linear regression slope = %.6f'%slope[i-1])
        
        slope_mean = np.mean( slope[ (plot_start-1):plot_stop ] )
        slope_dev = np.std( slope[ (plot_start-1):plot_stop ] )
        print ('Mean slope = %.6f'%slope_mean)
        #print ('Slope std dev = %.6f'%slope_dev)
        
        ax1.set_ylabel('%s [%s]'%( y.getType(), y.getUnit() ) )
        ax1.set_xlabel(r'%s [%s]'%( x.getType(), x.getUnit() ))
        ax1.grid(True)
        pylab.legend( labels[ (plot_start-1):plot_stop ], loc='best' )
        #=======================================================================
        
        #== Adding title and saving to file (optional) =========================
        if(title):
            pylab.title(title)
        pylab.tight_layout()
            
        if(filename and self.picpath):
            pylab.savefig( '%s\\%s'%(self.picpath, filename) )
        return slope_mean
        #=======================================================================
        
    def CoefficientCalc(self, x, y, x_range, plot_nums, title = None, filename = None):
        #== Assertions =========================================================
        if ( type(x) is not MeasData ) or ( type(y) is not MeasData ):
            raise TypeError('x and y have to be objects of type MeasData.')
        #=======================================================================
        
        #== Finding edges ======================================================
        edge_idx = self.FindEdges(x.getData(), self.nstart, self.nstop, x_range[0], x_range[1])
        limit = len(edge_idx)
        
        if (limit % 2 != 0):
            raise ValueError('edge_idx must be an array of integers containing an even number of elements!')
        if ( np.max(plot_nums) > limit/2 ):
            raise ValueError('plot_nums elements have to be smaller than number of slopes in x_data!')
        
        labels = self.CreateLabels(limit)
            
        plot_start = plot_nums[0]
        plot_stop = plot_nums[1]
        #=======================================================================
        
        #== Plotting data ======================================================
        fig = pylab.figure()
        ax1 = fig.add_subplot(1,1,1)
        ax1.set_ylabel(r'%s coefficient of %s [%s/%s]'%(x.getType(), y.getType(), y.getUnit(), x.getUnit()))
        
        i = 0
        mean_coeff = [0.0] * int(limit / 2)
    
        for i in np.linspace(plot_start, plot_stop, plot_stop - plot_start + 1, endpoint = True, dtype = int) :
            y_tmp = y.getData()[ edge_idx[(i-1)*2] : edge_idx[(i-1)*2 + 1] ]     
            x_tmp = x.getData()[ edge_idx[(i-1)*2] : edge_idx[(i-1)*2 + 1] ]
            coeff = PhaseData.DiffCalculate(y_tmp, x_tmp, self.diffWindow)
            mean_coeff[i-1] = np.mean(coeff[self.diffWindow:-1])
            
            ax1.plot(x_tmp[self.diffWindow:-2], coeff[self.diffWindow:-1])
            #print (labels[i-1])
            
        print('Mean coeff: %.6f'%(np.mean(mean_coeff)) )
    
        ax1.set_xlabel(r'%s [%s]'%( x.getType(), x.getUnit() ) )
        ax1.grid(True)
        pylab.legend(labels[(plot_start-1):plot_stop], loc='best')
        #=======================================================================
        
        #== Adding title and saving to file (optional) =========================
        if(title):
            pylab.title(title)
        pylab.tight_layout()
            
        if(filename and self.picpath):
            pylab.savefig( '%s\\%s'%(self.picpath, filename) )
        #=======================================================================
    #===========================================================================
    
    #===========================================================================
    @staticmethod
    def VectorMatchIdx(v1, v2):
        #== Assertions =========================================================
        N = len(v1)
        M = len(v2)
        
        if(M < N):
            temp = v2
            v2 = v1
            v1 = temp
            N = len(v1)
            M = len(v2)
            
        #=======================================================================
        # if(M < N):
        #     raise ValueError('Vectors lengths are wrong... or something.')
        #=======================================================================
        
        if(N == M):
            print('Vectors have the same size. Optimization skipped.')
            idx = 0
            return idx
        #=======================================================================
        
        #== Optimization =======================================================
        else:
            lim = M - N + 1
            for i in range(lim):
                vdiff = v2[ i:N+i ] - v1
                vval = np.sum( np.square(vdiff) )
                
                if(not i):
                    vmin = vval
                    idx = i
                else:
                    if(vval < vmin):
                        vmin = vval
                        idx = i
                    else:
                        break
            return idx
        #=======================================================================
    #===========================================================================
    
    #===========================================================================
    def MatchTempData(self, td1, td2, ncycle):
        #== Assertions =========================================================
        if(type(td1) is not MeasData) or (type(td2) is not MeasData):
            raise TypeError('Both td arguments must be objects of type MeasData.')
        if( [td1.getType(), td1.getUnit()] != [td2.getType(), td2.getUnit()] ):
            raise TypeError('Both MeasData arguments must have same datatypes and units.')
        if( type(ncycle) is not int ):
            raise TypeError('nslope argument has to be of type int.')
        #=======================================================================
        
        #== Setting variables ==================================================
        N1 = len(td1)
        N2 = len(td2)
        if(N1 > N2):
            raise ValueError('Second argument must have greater size than first.')
        
        idx = []
        if(ncycle > N1):
            print('Given data is too short or ncycle argument is too big.')
            return idx
        
        totalcycles = int(N2/N1)
        #=======================================================================
        
        #== Matching data ======================================================
        for i in range(totalcycles):
            nshift = self.VectorMatchIdx(td1.getData(), td2.getData()[ i*ncycle: ])
            idx.append([ i*ncycle + nshift, i*ncycle + nshift + N1 ])
        return idx
        #=======================================================================
    #===========================================================================
    
    #===========================================================================
    def MatchDataByTemp(self, timeset, dataset, tempset, ncycle, nstop):
        #== Assertions =========================================================
        Ndata = len(dataset)
        Ntemp = len(tempset)
        #if( Ndata < 2 ) or ( Ntemp < 2 ):
            #raise ValueError('Both arguments have to be lists containing at least 2 elements.')
        if(Ndata != Ntemp):
            raise ValueError('Both argument lists must have the same size.')
        
        for i in range(Ndata):
            if( type(dataset[i]) is not MeasData ) or ( type(tempset[i]) is not MeasData ):
                raise TypeError('Every element of given lists has to be of type MeasData.')
            if( len(dataset[i]) != len(tempset[i]) ):
                raise ValueError('Corresponding list elements should have the same datasize.')
            
        N = np.zeros(Ndata)
        for i in range(Ndata):
            N[i] = len(dataset[i])
        idxmin = np.argmin(N)
        Nmin = N[idxmin]
        if(Nmin < ncycle) or (Nmin < nstop):
            raise ValueError('All given datasets has to have length greater or equal to ncycle and nstop values.')
        #=======================================================================
        
        #== Matching datasets ==================================================
        #data0 = dataset[idxmin][:ncycle]
        time0 = timeset[idxmin][:nstop]
        temp0 = tempset[idxmin][:nstop]
        dataout = []
        tempout = []#tempset[idxmin]
        for i in range(Ndata):
            idxmatch = self.MatchTempData(temp0, tempset[i], ncycle)
            dout = []
            tout = []
            for j in range( len(idxmatch) ):
                dout.append( dataset[i][ idxmatch[j][0] : idxmatch[j][1] ] )
                tout.append( tempset[i][ idxmatch[j][0] : idxmatch[j][1] ] )
            dataout.append(dout)
            tempout.append(tout)
        #=======================================================================
        return [dataout, tempout, time0, temp0]
    #===========================================================================
        
 
if __name__ == '__main__':
    pass