#-------------------------------------------------------------------------------
# Name:        PhaseCalc_main
# Purpose:     Script for plotting the measurement data acquired from phase detector.
#
# Author:      Kamil Sapor
#
# Created:     16-04-2017
#-------------------------------------------------------------------------------

import os
import pylab
import copy
import numpy as np

from PhaseData import MeasData, PhaseData, PhaseDataInitStruct
import PhasePlot as phpl

#== CONSTANTS ==================================================================
FREQ = 704.42          #Needs to be specified in MHz
DETECTOR_FILENAME = 'GreenDetNew_704MHz.npz'
DF1 = 'DIV12-wil_out1-1_704M_9_11_2018_data.txt'
DF2 = 'DIV12-wil_out1-1_704M_9_14_2018_data.txt'
DF3 = 'DIV12-wil_out1-1rev_704M_9_12_2018_data.txt'
DF4 = 'DIV12-wil_out1-1rev_704M_9_13_2018_data.txt'
DF5 = 'DIV12-wil_out1-1rev_704M_9_24_2018_data.txt'
#===============================================================================

#== PATHS ======================================================================
CWD = os.getcwd()
DATAPATH = '%s\\data'%CWD
PICPATH = '%s\\pictures'%CWD
DETPATH = '%s\\instruments'%CWD

# "For %s at %.2f MHz."%( DUT_NAME, FREQ )
DUTNAME = 'Wilkinson DIV12\'s OUT1 reversed'
#DUTNAME = 'Setup reversed'
#===============================================================================

#== MAIN =======================================================================
pdinit = PhaseDataInitStruct()                 
pdinit.datapath = DATAPATH
pdinit.detpath = DETPATH
pdinit.detFilename = DETECTOR_FILENAME
#===============================================================================

#== DATA COLUMNS ===============================================================
pdinit.phaseColumn = 4
pdinit.magnitudeColumn = 1
pdinit.tempColumn1 = 2
pdinit.tempColumn2 = 3
pdinit.humidityColumn = 1
pdinit.voltageColumn = 1
pdinit.pressureColumn = 1
#===============================================================================

#== PHASEPLOT PARAMS ===========================================================
timeInt = 10
Nslope = int(3600/timeInt)
slopes_per_cycle = 4

Ncycle = Nslope*slopes_per_cycle
diffWindow = 10
n_start = 110                       
n_stop = Ncycle + Nslope   
#===============================================================================

#== OTHER PARAMS ===============================================================
pdinit.windowSize = 50
pdinit.phaseWindow = 50
pdinit.nstart = n_start

pdinit.freq = FREQ
pdinit.timeInt = timeInt
pdinit.phaseSlopeType = 'Negative'
pdinit.length_cable = 1
#===============================================================================

#== Loading data ===============================================================
pdinit.datafile_name = DF1
pd1 = PhaseData(pdinit)

pdinit.datafile_name = DF2
pd2 = PhaseData(pdinit)

pdinit.datafile_name = DF3
pdrev1 = PhaseData(pdinit)

pdinit.datafile_name = DF4
pdrev2 = PhaseData(pdinit)

pdinit.datafile_name = DF5
pdrev3 = PhaseData(pdinit)
#===============================================================================

#== Preparing datasets =========================================================
pp = phpl.PhasePlot(n_start, n_stop, diffWindow, picpath = PICPATH)

pd1.temp1.setLabel('Oven temp pd1')
pd1.temp2.setLabel('PhaseDet temp pd1')

pd2.temp1.setLabel('Oven temp pd2')
pd2.temp2.setLabel('PhaseDet temp pd2')

pdrev1.temp1.setLabel('Oven temp pdrev1')
pdrev1.temp2.setLabel('PhaseDet temp pdrev1')

pdrev2.temp1.setLabel('Oven temp pdrev2')
pdrev2.temp2.setLabel('PhaseDet temp pdrev2')

pdrev3.temp1.setLabel('Oven temp pdrev3')
pdrev3.temp2.setLabel('PhaseDet temp pdrev3')

time1 = [pd1.time, pd2.time]
tsamp1 = [pd1.samples, pd2.samples]
dataset1 = [pd1.phasedeg, pd2.phasedeg]
tempset1 = [pd1.temp1, pd2.temp1]

time2 = [pdrev1.time, pdrev2.time, pdrev3.time]
tsamp2 = [pdrev1.samples, pdrev2.samples, pdrev3.samples]
dataset2 = [pdrev1.phasedeg, pdrev2.phasedeg, pdrev3.phasedeg]
tempset2 = [pdrev1.temp1, pdrev2.temp1, pdrev3.temp1]
#===============================================================================

#== Matching vectors ===========================================================
[dataout1, tempout1, time10, temp10] = pp.MatchDataByTemp(time1, dataset1, tempset1, Ncycle, n_stop)

phase1 = []
phaselabels = [ 'norm1', 'norm2' ]
for i in range( len(dataout1) ):
    for j in range( len(dataout1[i]) ):
        phase1.append(dataout1[i][j])
        phase1[-1].setLabel(phaselabels[i] + ' cycle %d'%j)
        
dset1 = copy.copy(phase1[:10])
dset1.append(temp10)
#===============================================================================

#===============================================================================
[dataout2, tempout2, time20, temp20] = pp.MatchDataByTemp(time2, dataset2, tempset2, Ncycle, n_stop)

phase2 = []
phaselabels = [ 'rev1', 'rev2', 'rev3' ]
for i in range( len(dataout2) ):
    for j in range( len(dataout2[i]) ):
        phase2.append(dataout2[i][j])
        phase2[-1].setLabel(phaselabels[i] + ' cycle %d'%j)
        
dset2 = copy.copy(phase2[:10])
dset2.append(temp20)
#===============================================================================

#== DataVsTime =================================================================
DUTNAME = 'Wilkinson DIV12\'s OUT1'
tit1 = "Phase difference and temperature changes over time.\n%s at %.2fMHz."%(DUTNAME, FREQ)
#pp.DataVsTime( pd1.time, [pd1.phasedeg, pd1.temp1])
#pp.DataVsTime( pd2.time, [pd2.phasedeg, pd2.temp1])
pp.DataVsTime(time10, dset1, title = tit1, filename = 'wilkinson_norm_spread.png')

DUTNAME = 'Wilkinson DIV12\'s OUT1 reversed'
tit2 = "Phase difference and temperature changes over time.\n%s at %.2fMHz."%(DUTNAME, FREQ)
pp.DataVsTime(time20, dset2, title = tit2, filename = 'wilkinson_rev_spread.png')

#===============================================================================
#===============================================================================

#===============================================================================

#== DataVsData =================================================================
#===============================================================================
print('======================================================')
print('DRIFT CALCULATION FOR NORM:')
slope1 = []
for phase in phase1:
    slope1.append( pp.DataVsData(temp10, phase, [22, 27]) )
    pylab.close()
print('======================================================')

print('DRIFT CALCULATION FOR REV:')
slope2 = []
for phase in phase2:
    slope2.append( pp.DataVsData(temp20, phase, [22, 27]) )
    pylab.close()
print('======================================================')

#===============================================================================
d1min = np.min( slope1 )
d1max = np.max( slope1 )

drift1_pp = d1max - d1min
drift1_rms = np.std( slope1 )
print('NORM drift')
print('Peak-peak value: %.6f'%(drift1_pp) )
print('RMS value: %.6f'%(drift1_rms) )
print('======================================================')
#===============================================================================
d2min = np.min( slope2 )
d2max = np.max( slope2 )

drift2_pp = d2max - d2min
drift2_rms = np.std( slope2 )
print('\nREV drift')
print('Peak-peak value: %.6f'%(drift2_pp) )
print('RMS value: %.6f'%(drift2_rms) )
print('======================================================')
#===============================================================================
#===============================================================================

#== Differential drift calculation =============================================
diff_drift = []
for p1 in phase1:
    for p2 in phase2:
        diff_drift.append( pp.DataVsData(temp10, (p2-p1).scaleData(0.5, 'deg'), [22, 27]) )
        pylab.close()

diff_min = np.min(diff_drift)
diff_max = np.max(diff_drift)

diff_pp = diff_max - diff_min
diff_rms = np.std(diff_drift)

print('\nDifferential drift')
print('MIN: %.6f'%diff_min)
print('MAX: %.6f'%diff_max)
print('Peak-Peak: %.6f'%diff_pp)
print('RMS: %.6f'%diff_rms)
print('======================================================')
#===============================================================================

#== CoefficientCalc ============================================================
#pp.CoefficientCalc(pd1.temp1, phase11, [22, 28], plot_nums = [1, 8])
#pp.CoefficientCalc(pd1.temp1, phase12, [22, 28], plot_nums = [1, 8])
#pp.CoefficientCalc(pd1.temp1, phase21, [22, 28], plot_nums = [1, 8])
#pp.CoefficientCalc(pd1.temp1, phase22, [22, 28], plot_nums = [1, 8])
#===============================================================================

pylab.show()

#===============================================================================